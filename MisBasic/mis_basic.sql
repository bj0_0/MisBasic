--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: authority; Type: TABLE; Schema: public; Owner: mis_basic; Tablespace: 
--

CREATE TABLE authority (
    authority_function character varying(50) NOT NULL,
    authority_name character varying(50) NOT NULL,
    sort_id integer NOT NULL,
    is_public boolean NOT NULL
);


ALTER TABLE authority OWNER TO mis_basic;

--
-- Name: menu; Type: TABLE; Schema: public; Owner: mis_basic; Tablespace: 
--

CREATE TABLE menu (
    id character varying(50) NOT NULL,
    p_id character varying(50),
    text character varying(100),
    url character varying(500)
);


ALTER TABLE menu OWNER TO mis_basic;

--
-- Name: operator; Type: TABLE; Schema: public; Owner: mis_basic; Tablespace: 
--

CREATE TABLE operator (
    operator_id character varying(50) NOT NULL,
    operator_name character varying(50) NOT NULL,
    password character varying(200) NOT NULL,
    password_expiry_date date NOT NULL,
    role_id integer NOT NULL,
    memo character varying(200) NOT NULL,
    visible boolean NOT NULL
);


ALTER TABLE operator OWNER TO mis_basic;

--
-- Name: role; Type: TABLE; Schema: public; Owner: mis_basic; Tablespace: 
--

CREATE TABLE role (
    role_id integer NOT NULL,
    role_name character varying(50) NOT NULL,
    start_url character varying(500)
);


ALTER TABLE role OWNER TO mis_basic;

--
-- Name: role_authority; Type: TABLE; Schema: public; Owner: mis_basic; Tablespace: 
--

CREATE TABLE role_authority (
    role_id integer NOT NULL,
    authority_function character varying(500) NOT NULL
);


ALTER TABLE role_authority OWNER TO mis_basic;

--
-- Name: role_role_id_seq; Type: SEQUENCE; Schema: public; Owner: mis_basic
--

CREATE SEQUENCE role_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_role_id_seq OWNER TO mis_basic;

--
-- Name: role_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mis_basic
--

ALTER SEQUENCE role_role_id_seq OWNED BY role.role_id;


--
-- Name: role_start_url; Type: TABLE; Schema: public; Owner: mis_basic; Tablespace: 
--

CREATE TABLE role_start_url (
    start_url character varying(50) NOT NULL,
    url_description character varying(50)
);


ALTER TABLE role_start_url OWNER TO mis_basic;

--
-- Name: role_id; Type: DEFAULT; Schema: public; Owner: mis_basic
--

ALTER TABLE ONLY role ALTER COLUMN role_id SET DEFAULT nextval('role_role_id_seq'::regclass);


--
-- Data for Name: authority; Type: TABLE DATA; Schema: public; Owner: mis_basic
--

INSERT INTO authority VALUES ('change_pass', '更改密码', 99, true);
INSERT INTO authority VALUES ('SysMain', '主菜单', 0, true);
INSERT INTO authority VALUES ('InvalidOperator', '无效操作员列表', 98, false);
INSERT INTO authority VALUES ('operator', '操作员维护', 97, false);
INSERT INTO authority VALUES ('role', '角色维护', 96, false);


--
-- Data for Name: menu; Type: TABLE DATA; Schema: public; Owner: mis_basic
--

INSERT INTO menu VALUES ('90', '0', '系统管理', NULL);
INSERT INTO menu VALUES ('99', '0', '无效数据', NULL);
INSERT INTO menu VALUES ('900', '90', '角色维护', 'Base/role.aspx');
INSERT INTO menu VALUES ('902', '90', '更改密码', 'Base/change_pass.aspx');
INSERT INTO menu VALUES ('901', '90', '操作员维护', 'Base/operator.aspx');
INSERT INTO menu VALUES ('990', '99', '无效操作员', 'InvalidData/InvalidOperator.aspx');


--
-- Data for Name: operator; Type: TABLE DATA; Schema: public; Owner: mis_basic
--

INSERT INTO operator VALUES ('admin', '超级用户', 'u4LSoHwL7pEuf+h5leLN8Q==', '2015-06-05', 0, '备注', true);
INSERT INTO operator VALUES ('test', '测试用户', 'u4LSoHwL7pEuf+h5leLN8Q==', '2015-06-12', 5, '测试', true);


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: mis_basic
--

INSERT INTO role VALUES (0, '系统管理员', 'SysMain.aspx');
INSERT INTO role VALUES (5, '测试组', 'SysMain.aspx');


--
-- Data for Name: role_authority; Type: TABLE DATA; Schema: public; Owner: mis_basic
--

INSERT INTO role_authority VALUES (0, 'role');
INSERT INTO role_authority VALUES (0, 'operator');
INSERT INTO role_authority VALUES (0, 'InvalidOperator');
INSERT INTO role_authority VALUES (5, 'role');
INSERT INTO role_authority VALUES (5, 'operator');


--
-- Name: role_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mis_basic
--

SELECT pg_catalog.setval('role_role_id_seq', 5, true);


--
-- Data for Name: role_start_url; Type: TABLE DATA; Schema: public; Owner: mis_basic
--

INSERT INTO role_start_url VALUES ('SysMain.aspx', '主菜单界面');


--
-- Name: pk_operator_id; Type: CONSTRAINT; Schema: public; Owner: mis_basic; Tablespace: 
--

ALTER TABLE ONLY operator
    ADD CONSTRAINT pk_operator_id PRIMARY KEY (operator_id);


--
-- Name: px_authority_function; Type: CONSTRAINT; Schema: public; Owner: mis_basic; Tablespace: 
--

ALTER TABLE ONLY authority
    ADD CONSTRAINT px_authority_function PRIMARY KEY (authority_function);


--
-- Name: px_menu_id; Type: CONSTRAINT; Schema: public; Owner: mis_basic; Tablespace: 
--

ALTER TABLE ONLY menu
    ADD CONSTRAINT px_menu_id PRIMARY KEY (id);


--
-- Name: px_role_authority; Type: CONSTRAINT; Schema: public; Owner: mis_basic; Tablespace: 
--

ALTER TABLE ONLY role_authority
    ADD CONSTRAINT px_role_authority PRIMARY KEY (role_id, authority_function);


--
-- Name: px_role_id; Type: CONSTRAINT; Schema: public; Owner: mis_basic; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT px_role_id PRIMARY KEY (role_id);


--
-- Name: px_role_start_url; Type: CONSTRAINT; Schema: public; Owner: mis_basic; Tablespace: 
--

ALTER TABLE ONLY role_start_url
    ADD CONSTRAINT px_role_start_url PRIMARY KEY (start_url);


--
-- Name: fkey_role_authority_role; Type: FK CONSTRAINT; Schema: public; Owner: mis_basic
--

ALTER TABLE ONLY role_authority
    ADD CONSTRAINT fkey_role_authority_role FOREIGN KEY (role_id) REFERENCES role(role_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

