﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvalidOperator.aspx.cs" Inherits="MisBasic.Views.InvalidData.InvalidOperator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/cupertino/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="/css/MisBase.css" />
    <script type="text/javascript" src="/EasyUI/jquery.min.js"></script>
    <script type="text/javascript" src="/EasyUI/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/EasyUI/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" >
        $(document).ready(function () {
        });

        function SetVisible() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                $.messager.confirm('警 告', '恢复用户为有效用户？', function (r) {
                    if (r) {
                        $.post('/Service/Base/OperatorService.ashx?command=SetVisible', { Operator_id: row.Operator_id, Visible: true }, function (result) {
                            if (result.success) {
                                $('#dg').datagrid('reload'); // reload the user data
                            } else {
                                $.messager.show({
                                    title: '提示信息',
                                    msg: result.errorMsg,
                                    timeout: 2000,
                                    showType: 'fade',
                                    style: {
                                        right: '',
                                        bottom: ''
                                    }
                                });
                            }
                        }, 'json');
                    }
                });
            }
        }

        function formatPassword(val, row) {
            return '******';
        }

        function formatDate(val, row) {
            if (val == null || val == '') {
                return '';
            }
            var dt = parseToDate(val);
            return dt.format("yyyy-MM-dd");
        }

        function parseToDate(value) {
            if (value == null || value == '') {
                return undefined;
            }

            var dt;
            if (value instanceof Date) {
                dt = value;
            }
            else {
                if (!isNaN(value)) {
                    dt = new Date(value);
                }
                else if (value.indexOf('/Date') > -1) {
                    value = value.replace(/\/Date\((-?\d+)\)\//, '$1');
                    dt = new Date();
                    dt.setTime(value);
                } else if (value.indexOf('/') > -1) {
                    dt = new Date(Date.parse(value.replace(/-/g, '/')));
                } else {
                    dt = new Date(value);
                }
            }
            return dt;
        }

        Date.prototype.format = function (format) //author: meizz 
        {
            var o = {
                "M+": this.getMonth() + 1, //month 
                "d+": this.getDate(),    //day 
                "h+": this.getHours(),   //hour 
                "m+": this.getMinutes(), //minute 
                "s+": this.getSeconds(), //second 
                "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter 
                "S": this.getMilliseconds() //millisecond 
            };
            if (/(y+)/.test(format))
                format = format.replace(RegExp.$1,
                        (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(format))
                    format = format.replace(RegExp.$1,
                            RegExp.$1.length == 1 ? o[k] :
                                ("00" + o[k]).substr(("" + o[k]).length));
            return format;
        };
    </script>
</head>
<body class="easyui-layout">
    <div  data-options="region:'center',border:'true',title:''" style="padding:10px" >
        <table id="dg" title="无效系统操作员列表" class="easyui-datagrid"
	        data-options="
	        fit:true,
	        url:'/Service/Base/OperatorService.ashx?command=GetInvisibleAll',
	        toolbar:toolbar,
            rownumbers:false,
	        fitColumns:false,
            singleSelect:true,
            autoRowHeight:false,
            pagination:false,
            pageSize:10">
            <thead>
                <tr>
                    <th field="Operator_id" width="100">编号</th>
                    <th field="Operator_name" width="100">名称</th>
                    <th field="Password" width="150" formatter="formatPassword">密码</th>
                    <th field="Password_expiry_date" width="200" formatter="formatDate">密码有效日期</th>
                    <th field="Role_name" width="150">角色</th>
                    <th field="Memo" width="350">备注</th>
                    <%--<th field="Visible" width="80">有效</th>--%>
                </tr>
            </thead>
        </table>
        <div id="toolbar">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="SetVisible()">设置有效</a>
        </div>
    </div>
</body>
</html>
