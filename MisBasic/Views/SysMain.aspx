﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SysMain.aspx.cs" Inherits="MisBasic.Views.SysMain" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/cupertino/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/icon.css" />
    <%--<link rel="stylesheet" type="text/css" href="/css/MisBase.css" />--%>
    <script type="text/javascript" src="/EasyUI/jquery.min.js"></script>
    <script type="text/javascript" src="/EasyUI/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/EasyUI/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.post('/Service/Base/OperatorService.ashx?command=GetLoginOperator', function (result) {
                if (result.success) {
                    $("#Operator_Role").html(result.msg1);
                    if (result.msg2 != '') {
                        $.messager.show({
                            title: '提示信息',
                            msg: result.msg2,
                            timeout: 5000,
                            showType: 'slide',
                        });
                    }
                }
            }, 'json');
            
        });
        function addTab(title, url) {
            if(url !=""){
                $.post('/Service/Base/OperatorService.ashx?command=OperatorHaveFun', { Url: url }, function (result) {
                    if (result.success) {
                        if (url.replace(/^\s\s*/, '').replace(/\s\s*$/, '') != '') {
                            if ($('#tt').tabs('exists', title)) {
                                $('#tt').tabs('select', title);
                            } else {
                                var content = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:99.66%;"></iframe>';;
                                $('#tt').tabs('add', {
                                    title: title,
                                    content: content,
                                    closable: true
                                });
                            }
                        }
                    } else {
                        $.messager.show({
                            title: '提示信息',
                            msg: result.errorMsg,
                            timeout: 2000,
                            showType: 'fade',
                            style: {
                                right: '',
                                bottom: ''
                            }
                        });
                    }
                }, 'json');
            }
        }
    </script>
</head>
<body class="easyui-layout" >
    <div data-options="region:'north'" style="height:55px;background:#214D90 url(../images/login/bg.gif) repeat-x;padding:5px 20px 0px 10px;">
        	<table class="ke-zeroborder" bordercolor="#000000" style="width:100%;" border="0" cellspacing="0" cellpadding="2">
		<tbody>
			<tr>
				<td>
					<span style="color:#fff;font-family:KaiTi_GB2312;font-size:32px;"><em>MIS基础项目系统</em></span><br />
				</td>
				<td>
					<p align="right">
						<span style="color:#fff;"><label id="Operator_Role"></label></span>
					</p>
				</td>
			</tr>
		</tbody>
	</table>
    </div>
    <div data-options="region:'south'" style="height:30px;padding:5px 10px 0px 0px;">
        <div align="right">
	        <span style="color:#0066FF;font-family:FangSong_GB2312;font-size:12px;"><strong>北京基础科技有限责任公司制作</strong></span>
        </div>
    </div>
    <%--<div data-options="region:'east',title:'East',split:true" style="width:500px;"></div>--%>
    <div data-options="region:'west',title:'菜单栏',split:true" style="width:250px;">
        <ul class="easyui-tree" id="MenuTree" 
            data-options="url:'/Service/Base/MenuTree.ashx',onClick: function(node){addTab(node.text,node.attributes.url);}">
        </ul>
    </div>
    <div class="easyui-tabs" id="tt" data-options="region:'center',border:'true'" >

    </div>
</body>
</html>
