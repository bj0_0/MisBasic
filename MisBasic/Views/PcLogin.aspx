﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PcLogin.aspx.cs" Inherits="MisBasic.Views.PcLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>系统登录</title>
    <link rel="stylesheet" href="../css/alogin.css"/>
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/cupertino/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/icon.css" />
    <script type="text/javascript" src="../EasyUI/jquery.min.js"></script>
    <script type="text/javascript" src="../EasyUI/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/EasyUI/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginform input[name='operator_id']").focus();
        });
        function getQueryStringByName(name) {
            var result = location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
            if (result == null || result.length < 1) {
                return "";
            }
            return result[1];
        }
        function passKeypress(event) {
            if (event.keyCode == "13") {
                submitForm();
            }
        }
        function submitForm() {

            if ($("#operator_id").val() == '' || $("#password").val() == '') {
                $.messager.alert('错误信息', '请填写用户名和密码！', 'error');
                //alert('登录信息不完整！');
                return false;
            }
            var url = getQueryStringByName('url');
            $.ajax({
                type: "POST",
                url: "../Service/Base/OperatorService.ashx?command=LOGIN&prefix=&url=" + url,
                data: $("form#loginform").serialize(),
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        location.href = data.url;
                    } else {
                        $.messager.alert('错误信息', '登录失败，请检查用户、密码及有效期！', 'error');
                        //alert('登录失败，请检查用户或密码是否正确！');
                    }
                }
            });
        }
    </script>
</head>
<body>
    <div class="Main">
        <ul>
            <li class="top"></li>
            <li class="top2"></li>
            <li class="topA"></li>
            <li class="topB">
                <span>
                    <img src="../images/login/logo.gif" alt="" style="" />
                </span>
            </li>
            <li class="topC"></li>
            <li class="topD">
                <form id="loginform" method ="post">
                    <ul class="login">
				        <li/>					
                        <li>
					        <span class="left">用户名：</span> 
					        <span class="left" >
						        <input id="operator_id" type="text"  name ="operator_id" class="txt" />  
                            </span>
				        </li>
				        <br/> 
                        <li>
					        <span class="left">密 码：</span> 
					        <span class="left">
						        <input id="password" type="password"  name="password" class="txt" onkeypress="javascript:passKeypress(event)" />  
					        </span>
                        </li> 
                    </ul>
                </form>
            </li>
            <li class="topE"></li>
            <li class="middle_A"></li>
            <li class="middle_B"></li>
            <li class="middle_C">
                <span class="btn">
                    <img src="../images/login/btnlogin.gif" onclick="javascript:submitForm();" />
                </span>
            </li>

            <li class="middle_D"></li>
            <li class="bottom_A"></li>
            <li class="bottom_B">
            </li>
        </ul>
    </div>
</body>
</html>
