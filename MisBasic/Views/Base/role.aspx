﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="role.aspx.cs" Inherits="MisBasic.Views.Base.role" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/cupertino/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="/css/MisBase.css" />
    <script type="text/javascript" src="/EasyUI/jquery.min.js"></script>
    <script type="text/javascript" src="/EasyUI/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/EasyUI/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" >
        $(document).ready(function () {
        });
        var url;

        function newData() {
            $('#dlg').dialog('open').dialog('setTitle', '新  增');
            $('#Role_name').textbox('textbox').focus();
            $("#Role_name").textbox('setValue', '');
            url = '/Service/Base/RoleService.ashx?command=New';
        }

        function editData() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                $('#dlg').dialog('open').dialog('setTitle', '编  辑');
                $('#fm').form('load', row);
                $('#Role_name').textbox('textbox').focus();
                url = '/Service/Base/RoleService.ashx?command=UpDate&Role_id=' + row.Role_id;
            }
        }

        function saveData() {
            $('#fm').form('submit', {
                url: url,
                onSubmit: function () {
                    return $(this).form('validate');
                },
                success: function (result) {
                    var result = eval('(' + result + ')');
                    if (result.errorMsg) {
                        $.messager.show({
                            title: '提示信息',
                            msg: result.errorMsg,
                            timeout: 2000,
                            showType: 'fade',
                            style: {
                                right: '',
                                bottom: ''
                            }
                        });
                    } else {
                        $('#dlg').dialog('close'); // close the dialog
                        $('#dg').datagrid('reload'); // reload the user data
                    }
                }
            });
        }

        function destroyData() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                $.messager.confirm('警 告', '确实要删除数据？删除数据将不可恢复！', function (r) {
                    if (r) {
                        $.post('/Service/Base/RoleService.ashx?command=Delete',{ Role_id: row.Role_id }, function (result) {
                            if (result.success) {
                                $('#dg').datagrid('reload'); // reload the user data
                            } else {
                                $.messager.show({
                                    title: '提示信息',
                                    msg: result.errorMsg,
                                    timeout: 2000,
                                    showType: 'fade',
                                    style: {
                                        right: '',
                                        bottom: ''
                                    }
                                });
                            }
                        }, 'json');
                    }
                });
            }
        }

        function SaveAuthority()
        {
            var authorityData = $('#dg2').datagrid('getData');
            $.post('/Service/Base/RoleService.ashx?command=SetRoleAuthority', { Data: JSON.stringify(authorityData["rows"]) }, function (result) {
                if (result.success) {
                    $.messager.show({
                        title: '提示信息',
                        msg: '角色权限保存成功！',
                        timeout: 2000,
                        showType: 'fade',
                        style: {
                            right: '',
                            bottom: ''
                        }
                    });
                } else {
                    $.messager.show({
                        title: '提示信息',
                        msg: result.errorMsg,
                        timeout: 2000,
                        showType: 'fade',
                        style: {
                            right: '',
                            bottom: ''
                        }
                    });
                }
            }, 'json');
        }

        function SelectRow(rowIndex, rowData) {
            $('#dg2').datagrid({
                url: '/Service/Base/RoleService.ashx?command=GetRoleAuthority&Role_id=' + rowData.Role_id
            });  
        }

        function LoadSuccess(data) {
            if (data.rows.length == 0) {
                $('#dg2').datagrid('loadData', { total: 0, rows: [] });
            }
            else {
                $('#dg').datagrid("selectRow", 0);
            }
        }

        $.extend($.fn.datagrid.methods, {
            editCell: function (jq, param) {
                return jq.each(function () {
                    var opts = $(this).datagrid('options');
                    var fields = $(this).datagrid('getColumnFields', true).concat($(this).datagrid('getColumnFields'));
                    for (var i = 0; i < fields.length; i++) {
                        var col = $(this).datagrid('getColumnOption', fields[i]);
                        col.editor1 = col.editor;
                        if (fields[i] != param.field) {
                            col.editor = null;
                        }
                    }
                    $(this).datagrid('beginEdit', param.index);
                    var ed = $(this).datagrid('getEditor', param);
                    if (ed) {
                        if ($(ed.target).hasClass('textbox-f')) {
                            $(ed.target).textbox('textbox').focus();
                        } else {
                            $(ed.target).focus();
                        }
                    }
                    for (var i = 0; i < fields.length; i++) {
                        var col = $(this).datagrid('getColumnOption', fields[i]);
                        col.editor = col.editor1;
                    }
                });
            },
            enableCellEditing: function (jq) {
                return jq.each(function () {
                    var dg2 = $(this);
                    var opts = dg2.datagrid('options');
                    opts.oldOnClickCell = opts.onClickCell;
                    opts.onClickCell = function (index, field) {
                        if (opts.editIndex != undefined) {
                            if (dg2.datagrid('validateRow', opts.editIndex)) {
                                dg2.datagrid('endEdit', opts.editIndex);
                                opts.editIndex = undefined;
                            } else {
                                return;
                            }
                        }
                        dg2.datagrid('selectRow', index).datagrid('editCell', {
                            index: index,
                            field: field
                        });
                        opts.editIndex = index;
                        opts.oldOnClickCell.call(this, index, field);
                    }
                });
            }
        });

        $(function () {
            $('#dg2').datagrid().datagrid('enableCellEditing');
        })

        function FormatSelect(val, rec) {
            if (val == 'true') {
                return '<span style="color:green;">是</span>';
            }
            else {
                return '<span style="color:red;">否</span>';
            }
        }

    </script>
</head>
<body class="easyui-layout">
    <div data-options="region:'west',title:'',split:true " style="width:500px; padding:10px">
        <table id="dg" title="角色列表" class="easyui-datagrid"
	        data-options="
	        fit:true,
	        url:'/Service/Base/RoleService.ashx?command=GetAll',
	        toolbar:toolbar,
            rownumbers:false,
	        fitColumns:false,
            singleSelect:true,
            autoRowHeight:false,
            pagination:false,
            pageSize:10,
			onSelect:SelectRow,
			onLoadSuccess:LoadSuccess">	
            <thead>
                <tr>
                    <th data-options="field:'Role_id',hidden:true">编号</th>
                    <th data-options="field:'Role_name',width:260">角色名称</th>
                    <th data-options="field:'Start_url',hidden:true">起始页</th>
                    <th data-options="field:'Url_description',width:200">起始页</th>
                </tr>
            </thead>
        </table>
        <div id="toolbar">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newData()">新增</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editData()">编辑</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyData()">删除</a>
        </div>
         <div id="dlg" class="easyui-dialog" style="width:360px;height:auto;padding:10px 20px"
            closed="true" buttons="#dlg-buttons" >
            <div class="ftitle">角色维护</div>
            <form id="fm" method="post" novalidate>
                <div class="fitem">
                    <label>名  称：</label>
                    <input id="Role_name" name="Role_name" class="easyui-textbox" required="true" style="width:150px"/>
                </div>
                <div class="fitem">
                    <label>起 始 页：</label>
                    <input class="easyui-combobox" 
                        id="Start_url"	
                        name="Start_url"  
                        style="width:150px"
			            data-options="
					    url:'/Service/Base/RoleService.ashx?command=GetRoleStartUrl',
					    method:'get',
					    valueField:'Start_url',
					    textField:'Url_description',
					    panelHeight:'auto',
                        editable:false,
                        required:true"/>
                </div>
            </form>
        </div>
        <div id="dlg-buttons">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveData()">保存</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">放弃</a>
        </div>
    </div>
    <div  data-options="region:'center',border:'true',title:''" style="padding:10px" >
        <table id="dg2" title="角色权限功能列表" class="easyui-datagrid"
	        data-options="
	        fit:true,
	        url:'',
	        toolbar:toolbar,
            rownumbers:false,
	        fitColumns:false,
            singleSelect:true,
            autoRowHeight:false,
            pagination:false,
            pageSize:10">
            <thead>
                <tr>
                    <th data-options="field:'Role_id',width:80,hidden:true">编号</th>
                    <th data-options="field:'Authority_function',width:80,hidden:true">功能名称</th>
                    <th data-options="field:'Authority_name',width:300">功能名称</th>
                    <th data-options="field:'IsSelect',width:80,formatter:FormatSelect,editor:{type:'checkbox',options:{on:'true',off:'false'}}">有效</th>
                </tr>
            </thead>
        </table>
        <div id="toolbar2">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="SaveAuthority()">保存</a>
        </div>
    </div>    
</body>
</html>
