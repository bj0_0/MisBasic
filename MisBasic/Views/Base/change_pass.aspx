﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="change_pass.aspx.cs" Inherits="MisBasic.Views.Base.change_pass" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/cupertino/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="/css/MisBase.css" />
    <script type="text/javascript" src="/EasyUI/jquery.min.js"></script>
    <script type="text/javascript" src="/EasyUI/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/EasyUI/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" >
        $(document).ready(function () {
            $('#Old_Password').textbox('textbox').focus();
        });
        function saveData() {
            var old_pass = $("#Old_Password").textbox('getValue');
            var new_pass = $("#New_Password").textbox('getValue');
            var new_pass_confim = $("#New_Password_Confim").textbox('getValue');

            if (new_pass != new_pass_confim) {
                $.messager.show({
                    title: '提示信息',
                    msg: '“新的密码”与“密码确认”不一致，密码修改失败！',
                    timeout: 2000,
                    showType: 'slide',
                    style: {
                        right: '',
                        bottom: ''
                    }
                });
            } else if (new_pass == old_pass) {
                $.messager.show({
                    title: '提示信息',
                    msg: '“新的密码”与“原始密码”一致，密码修改失败！',
                    timeout: 2000,
                    showType: 'slide',
                    style: {
                        right: '',
                        bottom: ''
                    }
                });
            }
            else {
                $.post('/Service/Base/OperatorService.ashx?command=CheckPass', { password: old_pass }, function (result) {
                    if (result.success) {
                        change_pass();
                    } else {
                        $.messager.show({
                            title: '提示信息',
                            msg: result.errorMsg,
                            timeout: 2000,
                            showType: 'fade',
                            style: {
                                right: '',
                                bottom: ''
                            }
                        });
                    }
                }, 'json');
            }

        }

        function change_pass() {
            $('#fm').form('submit', {
                url: '/Service/Base/OperatorService.ashx?command=ChangePass',
                onSubmit: function () {
                    return $(this).form('validate');
                },
                success: function (result) {
                    var result = eval('(' + result + ')');
                    if (result.errorMsg) {
                        $.messager.show({
                            title: '提示信息',
                            msg: result.errorMsg,
                            timeout: 2000,
                            showType: 'fade',
                            style: {
                                right: '',
                                bottom: ''
                            }
                        });
                    } else {
                        $.messager.show({
                            title: '提示信息',
                            msg: '密码修改成功，密码有限期顺延完成！',
                            timeout: 2000,
                            showType: 'slide',
                            style: {
                                right: '',
                                bottom: ''
                            }
                        });
                        clearData();
                    }
                }
            });
        }

        function clearData() {
            $("#Old_Password").textbox('setValue', '');
            $("#New_Password").textbox('setValue', '');
            $("#New_Password_Confim").textbox('setValue', '');
        }
    </script>
</head>
<body>
    <div id="dlg" class="easyui-dialog" style="width:360px;height:auto;padding:10px 20px"
        closable="false"  buttons="#dlg-buttons" title="密码操作">
        <div class="ftitle">用户密码修改</div>
        <form id="fm" method="post" novalidate>
            <div class="fitem">
                <label>原始密码：</label>
                <input id="Old_Password" name="Old_Password" class="easyui-textbox" type="password" required="true" style="width:150px"/>
            </div>
            <div class="fitem">
                <label>新的密码：</label>
                <input id="New_Password" name="New_Password" class="easyui-textbox" type="password" required="true" style="width:150px"/>
            </div>
            <div class="fitem">
                <label>密码确认：</label>
                <input id="New_Password_Confim" name="New_Password_Confim" class="easyui-textbox" type="password" required="true" style="width:150px"/>
            </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveData()">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="clearData()">重置</a>
    </div>
</body>
</html>
