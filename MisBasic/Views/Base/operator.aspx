﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="operator.aspx.cs" Inherits="MisBasic.Views.Base._operator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/cupertino/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/EasyUI/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="/css/MisBase.css" />
    <script type="text/javascript" src="/EasyUI/jquery.min.js"></script>
    <script type="text/javascript" src="/EasyUI/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/EasyUI/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" >
        $(document).ready(function () {
        });
        var url;

        function newData() {
            $('#dlg').dialog('open').dialog('setTitle', '新  增');
            $('#Operator_id').textbox('readonly', false);
            $('#Operator_id').textbox('textbox').focus();
            $("#Operator_id").textbox('setValue', '');
            $("#Operator_name").textbox('setValue', '');
            $("#Password").textbox('setValue', '');
            $("#Memo").textbox('setValue', '');
            url = '/Service/Base/OperatorService.ashx?command=New';
        }

        function editData() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                $('#dlg').dialog('open').dialog('setTitle', '编  辑');
                $('#fm').form('load', row);
                $('#Operator_id').textbox('readonly', true);
                $('#Operator_name').textbox('textbox').focus();
                url = '/Service/Base/OperatorService.ashx?command=Update';
            }
        }

        function saveData() {
            $('#fm').form('submit', {
                url: url,
                onSubmit: function () {
                    return $(this).form('validate');
                },
                success: function (result) {
                    var result = eval('(' + result + ')');
                    if (result.errorMsg) {
                        $.messager.show({
                            title: '提示信息',
                            msg: result.errorMsg,
                            timeout: 2000,
                            showType: 'fade',
                            style: {
                                right: '',
                                bottom: ''
                            }
                        });
                    } else {
                        $('#dlg').dialog('close'); // close the dialog
                        $('#dg').datagrid('reload'); // reload the user data
                    }
                }
            });
        }

        function destroyData() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                $.messager.confirm('警 告', '确实要删除数据？删除数据将不可恢复！', function (r) {
                    if (r) {
                        $.post('/Service/Base/OperatorService.ashx?command=Delete', { Operator_id: row.Operator_id }, function (result) {
                            if (result.success) {
                                $('#dg').datagrid('reload'); // reload the user data
                            } else {
                                $.messager.show({
                                    title: '提示信息',
                                    msg: result.errorMsg,
                                    timeout: 2000,
                                    showType: 'fade',
                                    style: {
                                        right: '',
                                        bottom: ''
                                    }
                                });
                            }
                        }, 'json');
                    }
                });
            }
        }

        function SetVisible() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                $.messager.confirm('警 告', '设置用户无效后，用户将不能登陆。继续吗？', function (r) {
                    if (r) {
                        $.post('/Service/Base/OperatorService.ashx?command=SetVisible', { Operator_id: row.Operator_id, Visible: false }, function (result) {
                            if (result.success) {
                                $('#dg').datagrid('reload'); // reload the user data
                            } else {
                                $.messager.show({
                                    title: '提示信息',
                                    msg: result.errorMsg,
                                    timeout: 2000,
                                    showType: 'fade',
                                    style: {
                                        right: '',
                                        bottom: ''
                                    }
                                });
                            }
                        }, 'json');
                    }
                });
            }
        }

        function formatPassword(val, row) {
           return '******';
        }

        function formatDate(val, row) {
            if (val == null || val == '') {
                return '';
            }
            var dt = parseToDate(val);
            return dt.format("yyyy-MM-dd");
        }

        function parseToDate(value) {
            if (value == null || value == '') {
                return undefined;
            }

            var dt;
            if (value instanceof Date) {
                dt = value;
            }
            else {
                if (!isNaN(value)) {
                    dt = new Date(value);
                }
                else if (value.indexOf('/Date') > -1) {
                    value = value.replace(/\/Date\((-?\d+)\)\//, '$1');
                    dt = new Date();
                    dt.setTime(value);
                } else if (value.indexOf('/') > -1) {
                    dt = new Date(Date.parse(value.replace(/-/g, '/')));
                } else {
                    dt = new Date(value);
                }
            }
            return dt;
        }

        Date.prototype.format = function (format) //author: meizz 
        {
            var o = {
                "M+": this.getMonth() + 1, //month 
                "d+": this.getDate(),    //day 
                "h+": this.getHours(),   //hour 
                "m+": this.getMinutes(), //minute 
                "s+": this.getSeconds(), //second 
                "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter 
                "S": this.getMilliseconds() //millisecond 
            };
            if (/(y+)/.test(format))
                format = format.replace(RegExp.$1,
                        (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(format))
                    format = format.replace(RegExp.$1,
                            RegExp.$1.length == 1 ? o[k] :
                                ("00" + o[k]).substr(("" + o[k]).length));
            return format;
        };
    </script>
</head>
<body class="easyui-layout">
    <div  data-options="region:'center',border:'true',title:''" style="padding:10px" >
        <table id="dg" title="系统操作员列表" class="easyui-datagrid"
	        data-options="
	        fit:true,
	        url:'/Service/Base/OperatorService.ashx?command=GetAll',
	        toolbar:toolbar,
            rownumbers:false,
	        fitColumns:false,
            singleSelect:true,
            autoRowHeight:false,
            pagination:false,
            pageSize:10">
            <thead>
                <tr>
                    <th field="Operator_id" width="100">编号</th>
                    <th field="Operator_name" width="100">名称</th>
                    <th field="Password" width="150" formatter="formatPassword">密码</th>
                    <th field="Password_expiry_date" width="200" formatter="formatDate">密码有效日期</th>
                    <th field="Role_name" width="150">角色</th>
                    <th field="Memo" width="350">备注</th>
                    <%--<th field="Visible" width="80">有效</th>--%>
                </tr>
            </thead>
        </table>
        <div id="toolbar">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newData()">新增</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editData()">编辑</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyData()">删除</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="SetVisible()">设置无效</a>
        </div>
        <div id="dlg" class="easyui-dialog" style="width:360px;height:auto;padding:10px 20px"
            closed="true" buttons="#dlg-buttons">
            <div class="ftitle">用户信息</div>
            <form id="fm" method="post" novalidate>
                <div class="fitem">
                    <label>编  号：</label>
                    <input id="Operator_id" name="Operator_id" class="easyui-textbox" required="true" style="width:150px"/>
                </div>
                <div class="fitem">
                    <label>名  称：</label>
                    <input id="Operator_name" name="Operator_name" class="easyui-textbox" required="true" style="width:150px"/>
                </div>
                <div class="fitem">
                    <label>密  码：</label>
                    <input id="Password" name="Password" class="easyui-textbox" required="true" type="password" style="width:150px"/>
                </div>
                <div class="fitem">
                    <label>备  注：</label>
                    <input id="Memo" name="Memo" class="easyui-textbox" style="width:150px"/>
                </div>
                <div class="fitem">
                    <label>角  色：</label>
                    <input class="easyui-combobox"
                        id="Role_id"
                        name="Role_id"
                        style="width:150px"
                        data-options="
                        url:'/Service/Base/RoleService.ashx?command=GetAllComBox',
                        method:'get',
                        valueField:'Role_id',
                        textField:'Role_name',
                        panelHeight:'auto',
                        editable:false,
                        required:true"/>
                </div>
            </form>
        </div>
        <div id="dlg-buttons">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveData()">保存</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">放弃</a>
        </div>
    </div>
</body>
</html>
