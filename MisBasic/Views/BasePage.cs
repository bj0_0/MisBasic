﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MisBasic.Views
{
    public class BasePage : System.Web.UI.Page
    {
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            string url = Request.Url.LocalPath.Replace("/MisBasic/", "");

            if (Session["User"] == null || Session["Pass"].ToString() == "")
            {
                Response.Redirect(String.Format("~/Views/PcLogin.aspx?url=" + System.Web.HttpUtility.UrlEncode(url)));
            }
            else
            {
                String fun = url.Split('/')[url.Split('/').Length - 1].Split('.')[0];
                MisBasic.Dao.Base.Role_AuthorityDao dao = new MisBasic.Dao.Base.Role_AuthorityDao();
                if (!dao.OperatorHaveFun(Session["User"].ToString(), fun))
                {
                    Response.Redirect(String.Format("~/Views/PcLogin.aspx?url=" + System.Web.HttpUtility.UrlEncode(url)));
                }
            }
        }
    }
}