﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace MisBasic.Service
{
    public class Authorize : IRequiresSessionState
    {
        public static bool IsAuthorized(HttpContext context)
        {
            if (context.Session["User"] != null)
            {
                return true;
            }
            else
            {
                if (context.Request.QueryString["User"] != null)
                    return true;
                else
                    return false;
            }
            //return true;
        }

        public String GetLoginOperatorId(HttpContext context)
        {
            return context.Session["User"].ToString();
            //return "admin";
        }
    }
}