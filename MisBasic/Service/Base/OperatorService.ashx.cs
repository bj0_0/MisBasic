﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace MisBasic.Service.Base
{
    /// <summary>
    /// OperatorService 的摘要说明
    /// </summary>
    public class OperatorService : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            if (context.Request.QueryString["command"] != null)
            {
                String command = context.Request.QueryString["command"].ToString().ToUpper();
                if (command == "LOGIN")
                {
                    context.Response.Write(Login(context));
                }
                else
                {
                    if (Authorize.IsAuthorized(context))
                    {
                        context.Response.Write(ProcessData(context, command));
                    }
                }
            }

            //if (Authorize.IsAuthorized(context))
            //{
            //    if (context.Request.QueryString["command"] != null)
            //    {
            //        String command = context.Request.QueryString["command"].ToString().ToUpper();
            //        context.Response.Write(ProcessData(context, command));
            //    }
            //}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private String ProcessData(HttpContext context, String command)
        {
            String result = "";
            MisBasic.Dao.Base.OperatorDao dao = new Dao.Base.OperatorDao();
            switch (command.ToUpper())
            {
                case "NEW":
                    result = Save(context, false);
                    break;
                case "UPDATE":
                    result = Save(context, true);
                    break;
                case "GETBYID":
                    String operator_id = context.Request.QueryString["operator_id"].ToString();
                    result = dao.Get(operator_id);
                    break;
                case "GETALL":
                    result = dao.GetAll();
                    break;
                case "DELETE":
                    result = Delete(context);
                    break;
                case"OPERATORHAVEFUN":
                    result = OperatorHaveFun(context);
                    break;
                case "CHANGEPASS":
                    result = ChangePass(context);
                    break;
                case "CHECKPASS":
                    result = CheckPass(context);
                    break;
                case "GETLOGINOPERATOR":
                    result = GetLoginOperator(context);
                    break;
                case "GETINVISIBLEALL":
                    result = dao.GetInvisibleAll();
                    break;
                case "SETVISIBLE":
                    result = SetVisible(context);
                    break;
            }
            return result;
        }

        private String SetVisible(HttpContext context)
        {
            string operator_id = context.Request["Operator_id"].ToString();
            Boolean visible = Convert.ToBoolean(context.Request["Visible"].ToString());
            MisBasic.Dao.Base.OperatorDao dao = new Dao.Base.OperatorDao();
            if (Convert.ToInt32(dao.SetVisible(operator_id, visible)) > 0)
            {
                return "{\"success\":\"true\"}";
            }
            else
            {
                return "{\"errorMsg\":\"设置用户有效性失败！\"}";
            }
        }

        private String GetLoginOperator(HttpContext context)
        {
            Authorize authorize = new Authorize();
            String operator_id = authorize.GetLoginOperatorId(context);
            MisBasic.Dao.Base.OperatorDao dao = new Dao.Base.OperatorDao();
            List<String> msg=dao.GetLoginOperator(operator_id);
            return "{\"success\":\"true\",\"msg1\":\"" + msg[0] + "\",\"msg2\":\"" + msg[1] + "\"}";
        }

        private String CheckPass(HttpContext context)
        {
            Authorize authorize = new Authorize();
            String operator_id = authorize.GetLoginOperatorId(context);
            String password = context.Request["password"].ToString();
            MisBasic.Dao.Base.OperatorDao dao = new Dao.Base.OperatorDao();
            if (dao.Login(operator_id, password))
            {
                return "{\"success\":\"true\"}";
            }else
            {
                return "{\"errorMsg\":\"旧密码错误，密码修改失败！\"}";
            }
        }

        private String ChangePass(HttpContext context)
        {
            Authorize authorize = new Authorize();
            string operator_id = authorize.GetLoginOperatorId(context);
            string password = context.Request["New_Password"].ToString();
            MisBasic.Dao.Base.OperatorDao dao = new MisBasic.Dao.Base.OperatorDao();
            if (Convert.ToInt32( dao.ChangePassword(operator_id,password))>0)
            {
                return "{\"success\":\"true\"}";
            }
            else
            {
                return "{\"errorMsg\":\"密码修改失败！\"}";
            }
        }

        private String Delete(HttpContext context)
        {
            MisBasic.Domain.Base.Operator entity = new MisBasic.Domain.Base.Operator();
            entity.Operator_id = context.Request["Operator_id"].ToString();
            MisBasic.Dao.Base.OperatorDao dao = new MisBasic.Dao.Base.OperatorDao();
            if (Convert.ToInt32(dao.Delete(entity)) > 0)
            {
                return "{\"success\":\"true\"}";
            }
            else
            {
                return "{\"errorMsg\":\"删除失败！\"}";
            }

        }

        private String OperatorHaveFun(HttpContext context)
        {
            MisBasic.Dao.Base.Role_AuthorityDao dao = new MisBasic.Dao.Base.Role_AuthorityDao();
            String url=context.Request["Url"].ToString().Trim();
            String fun = url.Split('/')[url.Split('/').Length - 1].Split('.')[0];
            Authorize authorize = new Authorize();
            String operator_id = authorize.GetLoginOperatorId(context); 
            if (dao.OperatorHaveFun(operator_id, fun))
            {
                return "{\"success\":\"true\"}";
            }
            else
            {
                return "{\"errorMsg\":\"对不起您没使用该功能的权限！\"}";
            }
        }

        private String Save(HttpContext context, Boolean isUpdate)
        {
            MisBasic.Domain.Base.Operator entity = new MisBasic.Domain.Base.Operator();
            entity.Operator_id = context.Request["Operator_id"].ToString();
            entity.Operator_name = context.Request["Operator_name"].ToString();
            entity.Password = context.Request["Password"].ToString();
            entity.Memo = context.Request["Memo"].ToString();
            entity.Role_id = Convert.ToInt32(context.Request["Role_id"].ToString());
            MisBasic.Dao.Base.OperatorDao dao = new MisBasic.Dao.Base.OperatorDao();
            if (isUpdate)
            {
                if (Convert.ToInt32(dao.Update(entity)) > 0)
                {
                    return "{\"success\":\"true\"}";
                }
                else
                {
                    return "{\"errorMsg\":\"保存失败！\"}";
                }
            }else
            {
                if (Convert.ToInt32(dao.Save(entity)) > 0)
                {
                    return "{\"success\":\"true\"}";
                }
                else
                {
                    return "{\"errorMsg\":\"保存失败！\"}";
                }
            }


        }

        private String Login(HttpContext context)
        {
            String url = context.Request["url"].ToString();
            String operator_id = context.Request["operator_id"].ToString();
            String password = context.Request["password"].ToString();
            String prefix = context.Request["prefix"].ToString();
            MisBasic.Dao.Base.OperatorDao dao = new Dao.Base.OperatorDao();
            if (dao.Login(operator_id, password))
            {
                MisBasic.Dao.Base.RoleDao dao2 = new MisBasic.Dao.Base.RoleDao();
                String start_url = dao2.GetStartUrl(operator_id);
                if (start_url.ToUpper() != "False".ToUpper() && start_url.Trim() != "")
                {
                    context.Session["User"] = operator_id;
                    context.Session["Pass"] = password;
                    if (url.Trim() == "")
                        return "{\"success\":true,\"url\":\"" + prefix + start_url + "\"}";
                    else
                    {
                        String fun = GetAccessFun(url);
                        MisBasic.Dao.Base.Role_AuthorityDao dao3 = new Dao.Base.Role_AuthorityDao();
                        if (dao3.OperatorHaveFun(operator_id, fun))
                            return "{\"success\":true,\"url\":\"" + url + "\"}";
                        else
                            return "{\"success\":true,\"url\":\"" + prefix + start_url + "\"}";
                    }
                }
                else
                    return "{\"success\":false,\"url\":\"\"}";
            }
            else
            {
                return "{\"success\":false,\"url\":\"\"}";
            }
        }

        private String GetAccessFun(String url)
        {
            return url.Split('/')[url.Split('/').Length - 1].Split('.')[0];
        }
    }
}