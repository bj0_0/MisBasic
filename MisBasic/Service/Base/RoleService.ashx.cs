﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace MisBasic.Service.Base
{
    /// <summary>
    /// RoleService 的摘要说明
    /// </summary>
    public class RoleService : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            if (Authorize.IsAuthorized(context))
            {
                if (context.Request.QueryString["command"] != null)
                {
                    String command = context.Request.QueryString["command"].ToString().ToUpper();
                    context.Response.Write(ProcessData(context, command));
                }
            }
        }

        private String ProcessData(HttpContext context, String command)
        {
            String result = "";
            MisBasic.Dao.Base.RoleDao dao = new Dao.Base.RoleDao();
            switch (command.ToUpper())
            {
                case "NEW":
                    result = Save(context,false);
                    break;
                case "UPDATE":
                    result = Save(context,true);
                    break;
                case "DELETE":
                    result = Delete(context);
                    break;
                case "GETALL":
                    result = dao.GetAll();
                    break;
                case "GETROLEAUTHORITY":
                    result = dao.GetRole_Authority(Convert.ToInt32(context.Request.QueryString["Role_id"].ToString()));
                    break;
                case "SETROLEAUTHORITY":
                    result = SetRoleAuthority(context);
                    break;
                case "GETROLESTARTURL":
                    MisBasic.Dao.Base.Role_Start_urlDao urlDao = new Dao.Base.Role_Start_urlDao();
                    String json = urlDao.GetAll();
                    result = json.Substring(0, json.Length - 2) + ",\"selected\":true}]";
                    break;
                case "GETALLCOMBOX":
                    String json2 = dao.GetAll();
                    result = json2.Substring(0, json2.Length - 2) + ",\"selected\":true}]";
                    break;
            }
            return result;
        }

        private string Delete(HttpContext context)
        {
            MisBasic.Domain.Base.Role entity = new MisBasic.Domain.Base.Role();
            entity.Role_id = Convert.ToInt32(context.Request["Role_id"].ToString());
            MisBasic.Dao.Base.RoleDao dao = new MisBasic.Dao.Base.RoleDao();
            if (Convert.ToInt32(dao.Delete(entity)) > 0)
            {
                return "{\"success\":\"true\"}";
            }
            else
            {
                return "{\"errorMsg\":\"删除失败！\"}";
            }
                 
        }

        private String Save(HttpContext context,Boolean isUpdate)
        {
            MisBasic.Domain.Base.Role entity = new MisBasic.Domain.Base.Role();
            entity.Role_name = context.Request["Role_name"].ToString();
            entity.Start_url = context.Request["Start_url"].ToString();
            MisBasic.Dao.Base.RoleDao dao = new MisBasic.Dao.Base.RoleDao();
            if (isUpdate)
            {
                entity.Role_id = Convert.ToInt32(context.Request.QueryString["role_id"].ToString());
                if (Convert.ToInt32(dao.Update(entity)) > 0)
                {
                    return "{\"success\":\"true\"}";
                }
                else
                {
                    return "{\"errorMsg\":\"保存失败！\"}";
                }
            }
            else
            {
                entity.Role_id = -1;
                if (Convert.ToInt32(dao.Save(entity)) > 0)
                {
                    return "{\"success\":\"true\"}";
                }
                else
                {
                    return "{\"errorMsg\":\"保存失败！\"}";
                }
            }
        }
        
        private String SetRoleAuthority(HttpContext context)
        {
            MisBasic.Dao.Base.RoleDao dao = new MisBasic.Dao.Base.RoleDao();
            dao.SetRoleAuthority(context.Request["Data"].ToString());
            return "{\"success\":\"true\"}";
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}