﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.Domain.Base
{
    public class Authority
    {
        public String Authority_function { get; set; }
        public String Authority_name { get; set; }
        public Boolean Is_public { get; set; }
    }
}
