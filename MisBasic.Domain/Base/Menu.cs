﻿using System;

namespace MisBasic.Domain.Base
{
    public class Menu
    {
        public String Id { get; set; }
        public String P_id { get; set; }
        public String Text { get; set; }
        public String Url { get; set; }
    }
}
