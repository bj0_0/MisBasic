﻿using System;

namespace MisBasic.Domain.Base
{
    public class Role
    {
        public Int32 Role_id { get; set; }
        public String Role_name { get; set; }
        public String Start_url { get; set; }
        public string Url_description { get; set; }
    }
}
