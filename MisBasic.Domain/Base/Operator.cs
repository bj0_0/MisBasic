﻿using System;

namespace MisBasic.Domain.Base
{
    public class Operator
    {
        public String Operator_id { get; set; }
        public String Operator_name { get; set; }
        public String Password { get; set; }
        public DateTime Password_expiry_date { get; set; }
        public Int32 Role_id { get; set; }
        public String Memo { get; set; }
        public Boolean Visible { get; set; }
        public String Role_name { get; set; }
    }
}
