﻿using MySql.Data.MySqlClient;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.Dao.Helper
{
    public sealed class DBManager
    {
        public static IDbConnection GetConnection(DataProvider providerType, String connectionString)
        {
            IDbConnection iDbConnection;
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    iDbConnection = new SqlConnection(connectionString);
                    break;
                case DataProvider.Postgresql:
                    iDbConnection = new NpgsqlConnection(connectionString);
                    break;
                case DataProvider.Odbc:
                    iDbConnection = new OdbcConnection(connectionString);
                    break;
                case DataProvider.SQLite:
                    iDbConnection = new SQLiteConnection(connectionString);
                    break;
                case DataProvider.MySql:
                    iDbConnection = new MySqlConnection(connectionString);
                    break;
                default:
                    return null;
            }
            return iDbConnection;
        }

        public static IDbCommand GetCommand(DataProvider providerType)
        {
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    return new SqlCommand();
                case DataProvider.Postgresql:
                    return new NpgsqlCommand();
                case DataProvider.Odbc:
                    return new OdbcCommand();
                case DataProvider.SQLite:
                    return new SQLiteCommand();
                case DataProvider.MySql:
                    return new MySqlCommand();
                default:
                    return null;
            }
        }

        public static IDbDataAdapter GetDataAdapter(DataProvider providerType)
        {
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    return new SqlDataAdapter();
                case DataProvider.Postgresql:
                    return new NpgsqlDataAdapter();
                case DataProvider.Odbc:
                    return new OdbcDataAdapter();
                case DataProvider.SQLite:
                    return new SQLiteDataAdapter();
                case DataProvider.MySql:
                    return new MySqlDataAdapter();
                default:
                    return null;
            }
        }

        public static IDbTransaction GetTransaction(DataProvider providerType, String connectionString)
        {
            IDbConnection iDbConnection = GetConnection(providerType, connectionString);
            IDbTransaction iDbTransaction = iDbConnection.BeginTransaction();
            return iDbTransaction;
        }

        public static IDbDataParameter[] GetParameters(DataProvider providerType, int paramsCount)
        {
            IDbDataParameter[] idbParams = new IDbDataParameter[paramsCount];
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    for (int i = 0; i < paramsCount; i++)
                    {
                        idbParams[i] = new SqlParameter();
                    }
                    break;
                case DataProvider.Postgresql:
                    for (int i = 0; i < paramsCount; i++)
                    {
                        idbParams[i] = new NpgsqlParameter();
                    }
                    break;
                case DataProvider.Odbc:
                    for (int i = 0; i < paramsCount; i++)
                    {
                        idbParams[i] = new OdbcParameter();
                    }
                    break;
                case DataProvider.SQLite:
                    for (int i = 0; i < paramsCount; i++)
                    {
                        idbParams[i] = new SQLiteParameter();
                    }
                    break;
                case DataProvider.MySql:
                    for (int i = 0; i < paramsCount; i++)
                    {
                        idbParams[i] = new MySqlParameter();
                    }
                    break;
                default:
                    idbParams = null;
                    break;
            }
            return idbParams;
        }
    }
}
