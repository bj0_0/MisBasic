﻿
namespace MisBasic.Dao.Helper
{
    public enum DataProvider
    {
        SqlServer,
        Postgresql,
        Odbc,
        MySql,
        SQLite
    }
}
