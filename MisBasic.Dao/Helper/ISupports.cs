﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.Dao.Helper
{
    public interface ISupports<TEntity, TId>
    {
        TId Save(TEntity entity);
        String Update(TEntity entity);

        String Get(TId id);
        String GetAll();
        TEntity GetEntity(TId id);
        List<TEntity> GetAllEntity();

        string Delete(TEntity entity);
    }
}
