﻿using System;
using System.Configuration;

namespace MisBasic.Dao
{
    public class Common
    {
        public static String PasswordSalt = "MisBasic1234567890";

        public static String GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["SysDB"].ConnectionString.ToString();
        }
        public static Helper.DataProvider GetDataProvider()
        {
            return Helper.DataProvider.Postgresql;
        }
        public static Int32 GetPasswordExpiryDays()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["PasswordExpiryDays"].ToString());
        }
        public static Int32 GetPasswordExpiryPromptDays()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings["PasswordExpiryPromptDays"].ToString());
        }
    }
}
