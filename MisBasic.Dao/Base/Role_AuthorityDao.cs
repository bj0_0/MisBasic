﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.Dao.Base
{
    public class Role_AuthorityDao : Helper.CHelper<MisBasic.Domain.Base.Role_Authority>, Helper.ISupports<MisBasic.Domain.Base.Role_Authority, String>
    {
        public string Save(Domain.Base.Role_Authority entity)
        {
            string sql = string.Format(@"SELECT COUNT(*) FROM  role_authority
                                                WHERE authority_function='{0}' AND role_id={1}"
                        , entity.Authority_function,entity.Role_id);
            if (IsExist(sql))
            {
                return "0";
            }
            else
            {
                sql = string.Format(@"INSERT INTO   role_authority (
                                                    role_id , 
                                                    authority_function )
                                                VALUES (
                                                     {0}, 
                                                    '{1}')"
                                                    , entity.Role_id
                                                    , entity.Authority_function);
                return ExecuteSql(sql);
            }
        }

        public string Update(Domain.Base.Role_Authority entity)
        {
            throw new NotImplementedException();
        }

        public string Get(string id)
        {
            throw new NotImplementedException();
        }

        public string GetAll()
        {
            throw new NotImplementedException();
        }

        public Domain.Base.Role_Authority GetEntity(string id)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Base.Role_Authority> GetAllEntity()
        {
            throw new NotImplementedException();
        }

        public string Delete(Domain.Base.Role_Authority entity)
        {
            string sql = string.Format(@"DELETE FROM  role_authority
                                                WHERE role_id = {0}
                                                  AND authority_function = '{1}'"
                                                    , entity.Role_id
                                                    , entity.Authority_function);
            return ExecuteSql(sql);
        }

        public string DeleteByRoleId(Int32 role_id)
        {
            string sql = string.Format(@"DELETE FROM  role_authority
                                                WHERE role_id = {0}"
                        , role_id);
            return ExecuteSql(sql);
        }
        public List<Domain.Base.Role_Authority> GetByRoleId(Int32 role_id)
        {
            string sql = string.Format(@"SELECT     role_authority.role_id, 
                                                    role_authority.authority_function, 
                                                    authority.authority_name
                                           FROM     role_authority INNER JOIN authority ON role_authority.authority_function = authority.authority_function
                                          WHERE     role_authority.role_id = '{0}' ORDER BY authority.sort_id"
                , role_id);
            return GetTList(sql);
        }
//        public string GetStartUrl(string operator_id, string authority_function)
//        {
//            String sql = string.Format(@"SELECT authority.authority_id, 
//                                                authority.authority_function, 
//                                                authority.start_url
//                                           FROM authority
//                                    INNER JOIN  role ON authority.authority_id = role.authority_id 
//                                    INNER JOIN  operator ON role.role_id = operator.role_id
//                                         WHERE  operator.operator_id ='{0}'", operator_id);
//            Domain.Base.Role_Authority entity = GetTById(sql);
//            if (entity.Role_id == null)
//                return "False";
//            else
//                if (authority_function != "")
//                    if (entity.Authority_function.ToUpper().Contains(authority_function.ToUpper()))
//                        return entity.Role_id;
//                    else
//                        return "False";
//                else
//                    return entity.Role_id;

//        }

//        public string GetStartUrl(string operatorId)
//        {
//            return GetStartUrl(operatorId, "");
//        }

        public Boolean OperatorHaveFun(String operator_id, String fun)
        {
            MisBasic.Dao.Base.AuthorityDao dao = new Dao.Base.AuthorityDao();
            if (!dao.FunIsPublic(fun))
            {
                string sql = string.Format(@"SELECT     COUNT(*)
                                           FROM     role_authority  
                                                    INNER JOIN role ON role_authority.role_id = role.role_id 
                                                    INNER JOIN operator ON role.role_id = operator.role_id
                                          WHERE     operator.operator_id = '{0}'
                                            AND     role_authority.authority_function = '{1}'"
                                            , operator_id
                                            , fun);
                return IsExist(sql);
            }
            else
            {
                return true;
            }
        }
    }
}
