﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.Dao.Base
{
    public class MenuDao : Helper.CHelper<MisBasic.Domain.Base.Menu>, Helper.ISupports<MisBasic.Domain.Base.Menu, String>
    {
        public string Save(Domain.Base.Menu entity)
        {
            throw new NotImplementedException();
        }

        public string Update(Domain.Base.Menu entity)
        {
            throw new NotImplementedException();
        }

        public string Get(string id)
        {
            throw new NotImplementedException();
        }

        public string GetAll()
        {
            throw new NotImplementedException();
        }

        public Domain.Base.Menu GetEntity(string id)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Base.Menu> GetAllEntity()
        {
            throw new NotImplementedException();
        }

        public string Delete(Domain.Base.Menu entity)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Base.Menu> Get(string[] arg)
        {
            string sql = String.Format(@"SELECT  id
                                                ,p_id
                                                ,text
                                                ,url
                                            FROM menu
                                            WHERE p_id = '{0}' ORDER BY id"
                                                , arg[0]);
            return this.GetTList(sql);
        }
    }
}
