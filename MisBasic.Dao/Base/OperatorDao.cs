﻿using Elixis;
using Elixis.EncryptionOptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.Dao.Base
{
    public class OperatorDao : Helper.CHelper<MisBasic.Domain.Base.Operator>, Helper.ISupports<MisBasic.Domain.Base.Operator,String>
    {
        public string Save(Domain.Base.Operator entity)
        {
            AESEncryptor fAESEncryptor = new AESEncryptor(Common.PasswordSalt, AESBits.BITS256);

            string sql = string.Format(@"SELECT COUNT(*) FROM  operator
                                                WHERE operator_id='{0}'"
                                    , entity.Operator_id);
            if (IsExist(sql))
            {
                return "0";
            }
            else
            {
                sql = string.Format(@"INSERT INTO   operator
                                                    (operator_id
                                                    ,operator_name
                                                    ,password
                                                    ,password_expiry_date
                                                    ,role_id
                                                    ,memo
                                                    ,visible)
                                           VALUES
                                                    ('{0}'
                                                    ,'{1}'
                                                    ,'{2}'
                                                    ,'{3}'
                                                    , {4}
                                                    ,'{5}'
                                                    ,'true')"
                                                    , entity.Operator_id
                                                    , entity.Operator_name
                                                    , fAESEncryptor.Encrypt(entity.Password)
                                                    , DateTime.Today.AddDays(Common.GetPasswordExpiryDays()).ToString("yyyy-MM-dd")
                                                    , entity.Role_id
                                                    , entity.Memo);
                return ExecuteSql(sql);
            }
        }

        public string Update(Domain.Base.Operator entity)
        {
            AESEncryptor fAESEncryptor = new AESEncryptor(Common.PasswordSalt, AESBits.BITS256);
            Domain.Base.Operator oldEntity = GetEntity(entity.Operator_id);

            if (oldEntity.Password != entity.Password)
            {
                entity.Password = fAESEncryptor.Encrypt(entity.Password);
                entity.Password_expiry_date = DateTime.Today.AddDays(Common.GetPasswordExpiryDays());
            }
            else
            {
                entity.Password_expiry_date = oldEntity.Password_expiry_date;
            }


            string sql = string.Format(@"UPDATE 	operator
                                            SET 	operator_name = '{0}', 
	                                                password = '{1}', 
	                                                password_expiry_date = '{2}', 
	                                                role_id = {3}, 
	                                                memo = '{4}'
                                            WHERE  operator_id = '{5}' "
                                                , entity.Operator_name
                                                , entity.Password
                                                , entity.Password_expiry_date.ToString("yyyy-MM-dd")
                                                , entity.Role_id
                                                , entity.Memo
                                                , entity.Operator_id);
            return ExecuteSql(sql);
        }

        public string Get(string id)
        {
            String sql = string.Format(@"SELECT operator.operator_id, 
                                                operator.operator_name, 
                                                operator.password, 
                                                operator.password_expiry_date, 
                                                operator.role_id, 
                                                operator.memo, 
                                                operator.visible, 
                                                role.role_name
                                           FROM operator INNER JOIN  role ON operator.role_id = role.role_id
                                          WHERE operator.operator_id = '{0}' AND operator.visible = 'true'", id);
            return this.GetById(sql);
        }

        public string GetAll()
        {
            String sql = string.Format(@"SELECT operator.operator_id, 
                                                operator.operator_name, 
                                                operator.password, 
                                                operator.password_expiry_date, 
                                                operator.role_id, 
                                                operator.memo, 
                                                operator.visible, 
                                                role.role_name
                                           FROM operator INNER JOIN  role ON operator.role_id = role.role_id 
                                          WHERE operator.visible = 'true'
                                       ORDER BY operator.operator_id");
            return this.GetList(sql);
        }

        public Domain.Base.Operator GetEntity(string id)
        {
            String sql = string.Format(@"SELECT operator.operator_id, 
                                                operator.operator_name, 
                                                operator.password, 
                                                operator.password_expiry_date, 
                                                operator.role_id, 
                                                operator.memo, 
                                                operator.visible, 
                                                role.role_name
                                           FROM operator INNER JOIN  role ON operator.role_id = role.role_id
                                          WHERE operator.operator_id = '{0}' AND operator.visible = 'true'", id);
            return this.GetTById(sql);
        }

        public List<Domain.Base.Operator> GetAllEntity()
        {
            String sql = string.Format(@"SELECT operator.operator_id, 
                                                operator.operator_name, 
                                                operator.password, 
                                                operator.password_expiry_date, 
                                                operator.role_id, 
                                                operator.memo, 
                                                operator.visible, 
                                                role.role_name
                                           FROM operator INNER JOIN  role ON operator.role_id = role.role_id 
                                          WHERE operator.visible = 'true'
                                       ORDER BY operator.operator_id");
            return this.GetTList(sql);
        }

        public string Delete(Domain.Base.Operator entity)
        {
            if (entity.Operator_id.ToUpper() != "ADMIN")
            {
                string sql = string.Format(@"DELETE FROM  operator
                                                WHERE Operator_id = '{0}'"
                                        , entity.Operator_id);
                return ExecuteSql(sql);
            }
            else
            {
                return "0";
            }
        }

        public Boolean Login(String operator_id, String password)
        {
            AESEncryptor fAESEncryptor = new AESEncryptor(Common.PasswordSalt, AESBits.BITS256);
            Domain.Base.Operator entity = GetEntity(operator_id);
            if (entity.Operator_id == null)
                return false;
            else
                if (entity.Password == fAESEncryptor.Encrypt(password))
                    if (entity.Password_expiry_date > DateTime.Today)
                        return true;
                    else
                        return false;
                else
                    return false;
        }

        public string ChangePassword(String operator_id, String password)
        {
            AESEncryptor fAESEncryptor = new AESEncryptor(Common.PasswordSalt, AESBits.BITS256);
            string sql = string.Format(@"SELECT COUNT(*) FROM  operator
                                                WHERE operator_id='{0}'"
                        , operator_id);
            if (IsExist(sql))
            {
                sql = string.Format(@"UPDATE 	operator
                                        SET 	password = '{0}', 
	                                            password_expiry_date = '{1}'
                                        WHERE  operator_id = '{2}' "
                                            , fAESEncryptor.Encrypt(password)
                                            , DateTime.Today.AddDays(Common.GetPasswordExpiryDays()).ToString("yyyy-MM-dd")
                                            , operator_id);
                return ExecuteSql(sql);
            }
            else
            {
                return "0";
            }
        }

        public List<string> GetLoginOperator(string operator_id)
        {
            List<string> result = new List<string>();
            string sql = string.Format(@"SELECT COUNT(*) FROM  operator
                                                WHERE operator_id='{0}'"
            , operator_id);
            if (IsExist(sql))
            {
                Domain.Base.Operator entity = GetEntity(operator_id);
                result.Add(string.Format("用户：{0}&nbsp;&nbsp;&nbsp;角色：{1}", entity.Operator_name, entity.Role_name));
                TimeSpan ts=entity.Password_expiry_date-DateTime.Today;
                if (ts.Days <= Common.GetPasswordExpiryPromptDays())
                    if (ts.Days > 0)
                        result.Add(String.Format("您的密码还有{0}天就要过期了，请更换密码！", ts.Days));
                    else
                        result.Add(String.Format("您的密码已经过期{0}天了，请更换密码！", ts.Days * -1));
                else
                    result.Add("");
            }
            else
            {
                result.Add("用户：xxx&nbsp;&nbsp;&nbsp;角色：xxx");
                result.Add("");
            }
            return result;
        }

        public string GetInvisibleAll()
        {
            String sql = string.Format(@"SELECT operator.operator_id, 
                                                operator.operator_name, 
                                                operator.password, 
                                                operator.password_expiry_date, 
                                                operator.role_id, 
                                                operator.memo, 
                                                operator.visible, 
                                                role.role_name
                                           FROM operator INNER JOIN  role ON operator.role_id = role.role_id 
                                          WHERE operator.visible = 'false'
                                       ORDER BY operator.operator_id");
            return this.GetList(sql);
        }

        public string SetVisible(string operator_id,Boolean visible)
        {
            string sql = string.Format(@"UPDATE 	operator
                                            SET 	visible='{0}'
                                            WHERE  operator_id = '{1}' "
                                                , visible
                                                , operator_id);
            return ExecuteSql(sql);
        }
    }
}
