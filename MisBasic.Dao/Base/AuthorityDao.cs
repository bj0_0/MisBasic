﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.Dao.Base
{
    public class AuthorityDao : Helper.CHelper<MisBasic.Domain.Base.Authority>, Helper.ISupports<MisBasic.Domain.Base.Authority, String>
    {
        public string Save(Domain.Base.Authority entity)
        {
            throw new NotImplementedException();
        }

        public string Update(Domain.Base.Authority entity)
        {
            throw new NotImplementedException();
        }

        public string Get(string id)
        {
            throw new NotImplementedException();
        }

        public string GetAll()
        {
            String sql = string.Format(@"SELECT authority.authority_function, 
                                                authority.authority_name,
                                                authority.is_public
                                           FROM authority ORDER BY authority.sort_id");
            return this.GetList(sql);
        }

        public Domain.Base.Authority GetEntity(string id)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Base.Authority> GetAllEntity()
        {
            String sql = string.Format(@"SELECT authority.authority_function, 
                                                authority.authority_name,
                                                authority.is_public
                                           FROM authority ORDER BY authority.sort_id");
            return this.GetTList(sql);
        }

        public string Delete(Domain.Base.Authority entity)
        {
            throw new NotImplementedException();
        }

        public Boolean FunIsPublic(String fun)
        {
            string sql = string.Format(@"SELECT     COUNT(*)
                                           FROM     authority  
                                          WHERE     authority.is_public = 'true'
                                            AND     authority.authority_function = '{0}'"
                                        , fun);
            return IsExist(sql);
        }
    }
}
