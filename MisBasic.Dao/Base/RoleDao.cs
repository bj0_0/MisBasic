﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.Dao.Base
{
    public class RoleDao : Helper.CHelper<MisBasic.Domain.Base.Role>, Helper.ISupports<MisBasic.Domain.Base.Role, String>
    {
        class Role_Authority_Select : MisBasic.Domain.Base.Role_Authority
        {
            public String Authority_name { get; set; }
            public String IsSelect { get; set; }
        }

        public string Save(Domain.Base.Role entity)
        {
            string sql = string.Format(@"SELECT COUNT(*) FROM  role
                                                WHERE role_id='{0}'"
                                    , entity.Role_id);
            if (IsExist(sql))
            {
                return "0";
            }
            else
            {
                sql = string.Format(@"INSERT INTO role (
                                                        role_name, 
                                                        start_url)
                                                VALUES (
                                                        '{0}', 
                                                        '{1}')"
                                                        , entity.Role_name
                                                        , entity.Start_url);
                return ExecuteSql(sql);
            }
        }

        public string Update(Domain.Base.Role entity)
        {
            string sql = string.Format(@"UPDATE role
                                            SET role_name='{0}', 
	                                            start_url='{1}'
                                         WHERE 	role_id={2}"
                                                , entity.Role_name
                                                , entity.Start_url
                                                , entity.Role_id);
            return ExecuteSql(sql);
        }

        public string Get(string id)
        {
            String sql = string.Format(@"SELECT role.role_id, 
                                                role.role_name, 
                                                role.start_url,
                                                role_start_url.url_description
                                           FROM role INNER JOIN role_start_url ON role.start_url = role_start_url.start_url
                                          WHERE role.role_id = '{0}'", id);
            return this.GetById(sql);
        }

        public string GetAll()
        {
            String sql = string.Format(@"SELECT role.role_id, 
                                                role.role_name, 
                                                role.start_url,
                                                role_start_url.url_description
                                           FROM role INNER JOIN role_start_url ON role.start_url = role_start_url.start_url
                                       ORDER BY role.role_id");
            return this.GetList(sql);
        }

        public Domain.Base.Role GetEntity(string id)
        {
            String sql = string.Format(@"SELECT role.role_id, 
                                                role.role_name, 
                                                role.start_url,
                                                role_start_url.url_description
                                           FROM role INNER JOIN role_start_url ON role.start_url = role_start_url.start_url
                                          WHERE role.role_id = '{0}'", id);
            return this.GetTById(sql);
        }

        public List<Domain.Base.Role> GetAllEntity()
        {
            String sql = string.Format(@"SELECT role.role_id, 
                                                role.role_name, 
                                                role.start_url,
                                                role_start_url.url_description
                                           FROM role INNER JOIN role_start_url ON role.start_url = role_start_url.start_url
                                       ORDER BY role.role_id");
            return this.GetTList(sql);
        }

        public string Delete(Domain.Base.Role entity)
        {
            string sql = string.Format(@"DELETE FROM  role
                                                WHERE role_id = '{0}'"
                        , entity.Role_id);
            return ExecuteSql(sql);
        }

        public string GetStartUrl(string operator_id)
        {
            string sql = string.Format(@"SELECT     role.role_id, 
                                                    role.role_name, 
                                                    role.start_url
                                           FROM     role INNER JOIN operator ON role.role_id = operator.role_id
                                          WHERE     operator.operator_id = '{0}'"
                            , operator_id);
            Domain.Base.Role entity = GetTById(sql);
            return entity.Start_url;
        }

        public string GetRole_Authority(Int32 role_id)
        {
            AuthorityDao authority_dao = new AuthorityDao();
            List<MisBasic.Domain.Base.Authority> standard_authority_list = authority_dao.GetAllEntity();
            Role_AuthorityDao role_authority_dao = new Role_AuthorityDao();
            List<MisBasic.Domain.Base.Role_Authority> role_authority_list = role_authority_dao.GetByRoleId(role_id);
            List<Role_Authority_Select> role_authority_select_list = new List<Role_Authority_Select>();
            foreach (MisBasic.Domain.Base.Authority standard_authority in standard_authority_list)
            {
                if (!standard_authority.Is_public)
                {
                    Role_Authority_Select role_authority_select = new Role_Authority_Select();
                    role_authority_select.Authority_function = standard_authority.Authority_function;
                    role_authority_select.Authority_name = standard_authority.Authority_name;
                    role_authority_select.Role_id = role_id;
                    role_authority_select.IsSelect = "false";
                    foreach (MisBasic.Domain.Base.Role_Authority role_authority in role_authority_list)
                    {
                        if (role_authority.Authority_function == role_authority_select.Authority_function)
                        {
                            role_authority_select.IsSelect = "true";
                            break;
                        }
                    }
                    role_authority_select_list.Add(role_authority_select);
                }
            }
            return JsonConvert.SerializeObject(role_authority_select_list);
        }

        public void SetRoleAuthority(string role_authority_json)
        {
            List<Role_Authority_Select> role_authority_select_list = (List<Role_Authority_Select>)JsonConvert.DeserializeObject(role_authority_json, typeof(List<Role_Authority_Select>));
            foreach(Role_Authority_Select role_authority_select in role_authority_select_list)
            {
                MisBasic.Domain.Base.Role_Authority role_authority = new Domain.Base.Role_Authority();
                role_authority.Role_id = role_authority_select.Role_id;
                role_authority.Authority_function = role_authority_select.Authority_function;
                Role_AuthorityDao role_authority_dao = new Role_AuthorityDao();
                if (role_authority_select.IsSelect=="true")
                {
                    role_authority_dao.Save(role_authority);
                }
                else
                {
                    role_authority_dao.Delete(role_authority);
                }
            }
        }
    }
}
