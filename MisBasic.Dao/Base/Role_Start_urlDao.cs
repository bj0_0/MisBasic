﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.Dao.Base
{
    public class Role_Start_urlDao : Helper.CHelper<MisBasic.Domain.Base.Role_Start_url>, Helper.ISupports<MisBasic.Domain.Base.Role_Start_url, String>
    {
        public string Save(Domain.Base.Role_Start_url entity)
        {
            throw new NotImplementedException();
        }

        public string Update(Domain.Base.Role_Start_url entity)
        {
            throw new NotImplementedException();
        }

        public string Get(string id)
        {
            throw new NotImplementedException();
        }

        public string GetAll()
        {
            String sql = string.Format(@"SELECT start_url, url_description
                                           FROM role_start_url");
            return this.GetList(sql);
        }

        public Domain.Base.Role_Start_url GetEntity(string id)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Base.Role_Start_url> GetAllEntity()
        {
            String sql = string.Format(@"SELECT start_url, url_description
                                           FROM role_start_url");
            return this.GetTList(sql);
        }

        public string Delete(Domain.Base.Role_Start_url entity)
        {
            throw new NotImplementedException();
        }
    }
}
