﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.test
{
    public class RoleTest
    {
        public void Save()
        {
            MisBasic.Domain.Base.Role entity = new Domain.Base.Role();
            entity.Role_name = "系统管理员";
            entity.Start_url = "SysMain.aspx";

            MisBasic.Dao.Base.RoleDao dao = new Dao.Base.RoleDao();
            dao.Save(entity);
        }
    }
}
