﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisBasic.test
{
    public class OperatorTest
    {
        public void Save()
        {
            MisBasic.Domain.Base.Operator entity = new Domain.Base.Operator();
            entity.Operator_id = "admin";
            entity.Operator_name = "系统管理员";
            entity.Password = "pass";
            entity.Role_id = 0;
            entity.Memo = "备注";
            MisBasic.Dao.Base.OperatorDao dao = new Dao.Base.OperatorDao();
            dao.Save(entity);
        }
    }
}
